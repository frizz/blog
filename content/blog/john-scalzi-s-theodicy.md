+++
author = ["Michael Frizzell", ""]
categories = []
date = 2021-01-26T06:00:00Z
description = "The characters in John Scalzi's \"Redshirts\" want to know why they suffer. Just like we all do."
image = "/images/download.jpg"
tags = ["theology", "literature", "scifi"]
title = "John Scalzi's theodicy"

+++
John Scalzi’s [_Redshirts_](http://amzn.to/NkEBfs) is a novel with a familiar premise: what if the characters in a story realize they are, in fact, characters in a story? In this case, what if the (thinly veiled) redshirts from Star Trek, characters with whom the viewer is not familiar and who die a spectacular death, figure out they are characters in a pretty crappy TV show?

As far as premises go, this is quite familiar, as one of the characters even points out, but not necessarily bad. Scalzi, who wrote one of the best super-soldier-in-space novels, takes something familiar and transforms it into an excellent book. _Redshirts_ is, in parts, a meditation on free will, a theodicy, a love story, and one of the best books on writing you will ever read. It is that second part, the theodicy, that I’m interested in today.

A theodicy is an attempt to reconcile the existence of evil and human suffering with the omniscience, omnipotence and omnibenevolence of God. Within the world of _Redshirts_ the characters recognize they are surrounded by misery and suffering, but only for a select few. The Captain and other senior officers are rarely affected (with the exception of the lieutenant who is constantly getting hurt only to quickly recover in time for the next away mission), but the lowly ensigns are dying in droves. What’s worse, these redshirts are dying for no good reason in increasingly bizarre ways. One is killed by ice sharks, the next by crazy robots who were given weapons, and another by sandworms that are clearly ripped off from _Dune_.

Needless to say, life for these characters is something of a hell, even if Scalzi fills the story with his characteristic humor. They come to realize their lives are guided not by their own actions, or even the fate of a powerful deity, but by some hack writer who makes and breaks rules on a whim. A writer who kills anyone with an interesting back story for dramatic effect, to make an audience feel something right before a commercial break. They come to realize their lives are essentially meaningless, that they can and will be killed without a moment’s hesitation.

When the characters eventually face their creator (and of course they do), they confront him with their grim existence. They throw in his face that his lazy writing has actual consequences, that his use of tropes and shortcuts has ruined lives and is the cause for much suffering in the universe. The writer has no comeback, he lamely attempts to mount a defense, but even he knows it is futile. He is a bad writer who has never written anything good; he is a lazy creator and his creations are revolting.

Christians, when talking about the problem of evil, usually go to St. Paul’s letter to the Romans in defense of God. In the 9th chapter, Paul imagines a scenario where a human goes to God and asks “Why did you make me like this?” God’s response is to point out that he is the creator and has the right to do what he wants. But, Paul goes on, God does this for a reason, there is not a randomness to things but rather he is working things out to create for himself a people, the children of God. That’s all well and good if you have a benevolent god, but the characters here have probably the worst deity of all time.

So what do they do? In a dream (that may or may not actually be real), the characters appear to the writer and talk to him. All of the characters he has personally killed stand before him and give their demand: they want meaning to their lives. Their spokesperson says they don’t mind death and suffering, they recognize it as part of the universe, but what they don’t want is to die for no reason. They want their deaths to provide more than just a sense of danger for the Captain or to make the audience feel sad for all of 30 seconds. They want their lives to have real meaning and they want the writer to be the Creator he should be.

Ok, so there really isn’t a theodicy in _Redshirts_, at least not in the traditional sense. The characters want to figure out why their lives are full of suffering, but they don’t really need to reconcile that with a good god because they discover they don’t have anything like that. But in the end they come to a point where they _want_ something like a theodicy: they want to reconcile their deaths with a larger purpose. They want meaning. Just like we all do.